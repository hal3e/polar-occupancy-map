from util import *
from sklearn.utils import shuffle
from vehicle import Vehicle
from og_rep import OG

# %%
# Function for data generation
def get_data(size_train=1000, test_p=0.1, val_p=0.1, visualization=False, use_flip_augmentation=False):
    scenario_labels = ['driving_in_lane', 'lane_change', 'freelane_cut_in', 'freelane_cut_out', 'cut-in-out',
                   'ta-free-cut-out', 'ta-free-cut-in', 'ta-sw-cut-in', 'ta-sw-cut-out']

    # Initialize the placeholders for the data
    X_train, y_train, X_test, y_test, X_valid, y_valid = [], [], [], [], [], [],
    X_train_raw, X_test_raw, X_valid_raw = [], [], []

    max_duration = 20
    num_samples  = max_duration * 20 # we use 200 semples to make a better simulation but later we use every second element

    dt_ = max_duration / num_samples

    # Number of categories
    num_c = 9

    # Equaly divide between the number of scenarios
    size_train = size_train // (num_c - 2)

    test_size  = int(size_train * test_p)
    val_size   = int(size_train * val_p)
    train_size = size_train - test_size - val_size

    visualize_data = visualization

    act_start_t = 2.0
    act_end_t   = 4.0
# %%
    # Run the simulation for all classes
    # ---------------------------------------------- lane-change ----------------------------------------------
    i = 0
    print("lane-change")
    while(i < size_train):
        man_rand_ego = 1
        lane_other   = 2

        # Check if all conditions for this maneuver are met
        # If the ego vehicle is in the middle lane than chose either left or right lane change
        # If it is on the boarder lanes then assign the proper lane change maneuver
        maneuver = np.random.choice([1,2])
        if maneuver == 1:
            lane_other = 0

        s_rand_oc  = np.random.randint(-5, 35)
        vs_rand_oc = np.random.uniform(23, 31)

        # Assign random starting velocity for ego vehicle
        vs_rand_ego = np.random.uniform(25.0, 29.0)

        # Assign random maneuver start and duration
        act_start = np.random.uniform(4.0, max_duration * 0.35)
        act_dur   = np.random.uniform(act_start_t, act_end_t)

        # Initialize the vehicles
        vehicle_ego    = Vehicle(name='ego', dt=dt_, maneuver=maneuver, s=0, vs=vs_rand_ego, lane=man_rand_ego, lc_duration=act_dur, lc_start=act_start)
        vehicle_other  = Vehicle(name='ov1', dt=dt_, maneuver=0, s=s_rand_oc, vs=vs_rand_oc, lane=lane_other)

        # Run the simulation
        for _ in range(num_samples):
            vehicles = (vehicle_ego, vehicle_other)
            vehicle_ego.check_sensor(vehicles)
            vehicle_ego.update()
            vehicle_other.check_sensor(vehicles)
            vehicle_other.update()

        # Fill the training, validation and test dataset
        if i < train_size:
            X_train.append(vehicle_ego.get_poms2())
            X_train_raw.append(vehicle_ego.data_oc[:][::2])
            y_train.append(make_one_hot(vehicle_ego.get_label(), num_categ=num_c)[::2])
        elif i < train_size + val_size:
            X_valid.append(vehicle_ego.get_poms2())
            X_valid_raw.append(vehicle_ego.data_oc[:][::2])
            y_valid.append(make_one_hot(vehicle_ego.get_label(), num_categ=num_c)[::2])
        else:
            X_test.append(vehicle_ego.get_poms2())
            X_test_raw.append(vehicle_ego.data_oc[:][::2])
            y_test.append(make_one_hot(vehicle_ego.get_label(), num_categ=num_c)[::2])

        # Visualize data
        if visualize_data:
            a = vehicle_ego.get_data()
            b = vehicle_other.get_data()

            plot_pom(vehicle_ego.get_poms2())

            label = vehicle_ego.get_label()

            plot_scenario(dt_, [a, b])
            plot_labels(label[::2])

        i += 1

# %%
    # ---------------------------------------------- freelane-cut-in ----------------------------------------------
    i = 0
    print("freelane-cut-in")
    while(i < size_train):
        # Assign random starting lanes for the vehicles
        random_lanes = np.random.permutation([0, 1, 2])

        man_rand_ego = random_lanes[0]
        man_rand_oth = random_lanes[1]
        man_rand_oth2 = random_lanes[2]

        # Check if all conditions for this maneuver are met
        # If the ego vehicle is in the middle lane than chose either left or right lane change
        # If it is on the boarder lanes then assign the proper lane change maneuver
        if man_rand_ego == 1:
            maneuver = np.random.choice([1,2])
        elif man_rand_ego == 0:
            maneuver = 1
        else:
            maneuver = 2

        if maneuver == 1:
            lane_d = 1
        else:
            lane_d = -1

        final_lane = random_lanes[0] + lane_d

        # Assign random starting positions and velocity for other participants
        s_rand_oc  = np.random.randint(-25, 55, 2)
        vs_rand_oc = np.random.uniform(24, 31, 2)

        # Assign random starting velocity for ego vehicle
        vs_rand_ego = np.random.uniform(25.0, 29.0)

        # Assign random maneuver start and duration
        act_start = np.random.uniform(4.0, max_duration * 0.35)
        act_dur   = np.random.uniform(act_start_t, act_end_t)

        # Make sure that there is a vehicle in front of the lane after the lane change and position it in front
        for j in range(len(random_lanes[1:])):
            if random_lanes[j + 1] == final_lane:
                s_rand_oc[j] = np.random.randint(25, 50) + (act_start + 1) * vs_rand_ego - ((act_start + 1) * vs_rand_oc[j])

        # Initialize the vehicles
        vehicle_ego    = Vehicle(name='ego', dt=dt_, maneuver=maneuver, s=0, vs=vs_rand_ego, lane=man_rand_ego, lc_duration=act_dur, lc_start=act_start)
        vehicle_other  = Vehicle(name='ov1', dt=dt_, maneuver=0, s=s_rand_oc[0], vs=vs_rand_oc[0], lane=man_rand_oth)
        vehicle_other2 = Vehicle(name='ov2', dt=dt_, maneuver=0, s=s_rand_oc[1], vs=vs_rand_oc[1], lane=man_rand_oth2)

        # Run the simulation
        for _ in range(num_samples):
            vehicles = (vehicle_ego, vehicle_other, vehicle_other2)
            vehicle_ego.check_sensor(vehicles)
            vehicle_ego.update()
            vehicle_other.check_sensor(vehicles)
            vehicle_other.update()
            vehicle_other2.check_sensor(vehicles)
            vehicle_other2.update()

        # Fill the training, validation and test dataset
        ego_labels   = vehicle_ego.get_label()
        ego_labels_2 = np.copy(ego_labels)
        ego_labels[ ego_labels==1 ] = 2
        ego_labels_2[ ego_labels==2 ] = 3
        if i < train_size:
            # add org scen
            X_train.append(vehicle_ego.get_poms2())
            X_train_raw.append(vehicle_ego.data_oc[:][::2])
            y_train.append(make_one_hot(ego_labels, num_categ=num_c)[::2])
            # add flipped scen
            if use_flip_augmentation:
                X_train.append(vehicle_ego.get_poms2()[::-1])
                X_train_raw.append(vehicle_ego.data_oc_flip[:][::2][::-1])
                y_train.append(make_one_hot(ego_labels_2, num_categ=num_c)[::-2])
        elif i < train_size + val_size:
            X_valid.append(vehicle_ego.get_poms2())
            X_valid_raw.append(vehicle_ego.data_oc[:][::2])
            y_valid.append(make_one_hot(ego_labels, num_categ=num_c)[::2])
            # add flipped scen
            if use_flip_augmentation:
                X_valid.append(vehicle_ego.get_poms2()[::-1])
                X_valid_raw.append(vehicle_ego.data_oc_flip[:][::2][::-1])
                y_valid.append(make_one_hot(ego_labels_2, num_categ=num_c)[::-2])
        else:
            X_test.append(vehicle_ego.get_poms2())
            X_test_raw.append(vehicle_ego.data_oc[:][::2])
            y_test.append(make_one_hot(ego_labels, num_categ=num_c)[::2])
            # add flipped scen
            if use_flip_augmentation:
                X_test.append(vehicle_ego.get_poms2()[::-1])
                X_test_raw.append(vehicle_ego.data_oc_flip[:][::2][::-1])
                y_test.append(make_one_hot(ego_labels_2, num_categ=num_c)[::-2])

        # Visualize data
        if visualize_data:
            a = vehicle_ego.get_data()
            b = vehicle_other.get_data()
            c = vehicle_other2.get_data()

            plot_pom(vehicle_ego.get_poms2())
            plot_pom(X_train[-1])

            label = ego_labels
            plot_scenario(dt_, [a, b, c])
            plot_labels(label[::2])
        i += 1
#%%
    # ---------------------------------------------- freelane-cut-out ---------------------------------------------
    i = 0
    print("freelane-cut-out")
    while(i < size_train):
        # Assign random starting lanes for the vehicles
        lanes = [0, 1, 2]
        random_lane_oth2 = np.random.choice([0, 2])
        lanes.pop(random_lane_oth2)

        # Use remaining lanes for the free lane cut out
        random_lanes_eo = np.random.permutation(lanes)

        man_rand_ego = random_lanes_eo[1]
        man_rand_oth = random_lanes_eo[1]
        man_rand_oth2 = random_lane_oth2

        # Assign the proper lane change maneuver
        if random_lanes_eo[1] - random_lanes_eo[0] == -1:
            maneuver = 1
        else:
            maneuver = 2

        # Assign random starting positions and velocity for other participants
        s_rand_oc  = np.random.randint(-25, 50, 2)
        vs_rand_oc = np.random.uniform(24.5, 30.5, 2)

        # Assign random starting velocity for ego vehicle
        vs_rand_ego = np.random.uniform(25.0, 29.0)

        # Assign random maneuver start and duration
        act_start = np.random.uniform(4.0, max_duration * 0.35)
        act_dur   = np.random.uniform(act_start_t, act_end_t)

        s_rand_oc[0] = np.random.randint(30, 40)

        # Initialize the vehicles
        vehicle_ego    = Vehicle(name='ego', dt=dt_, maneuver=maneuver, s=0, vs=vs_rand_ego, lane=man_rand_ego, lc_duration=act_dur, lc_start=act_start)
        vehicle_other  = Vehicle(name='ov1', dt=dt_, maneuver=0, s=s_rand_oc[0], vs=vs_rand_oc[0], lane=man_rand_oth)
        vehicle_other2 = Vehicle(name='ov2', dt=dt_, maneuver=0, s=s_rand_oc[1], vs=vs_rand_oc[1], lane=man_rand_oth2)

        # Run the simulation
        for _ in range(num_samples):
            vehicles = (vehicle_ego, vehicle_other, vehicle_other2)
            vehicle_ego.check_sensor(vehicles)
            vehicle_ego.update()
            vehicle_other.check_sensor(vehicles)
            vehicle_other.update()
            vehicle_other2.check_sensor(vehicles)
            vehicle_other2.update()

        # Fill the training, validation and test dataset
        ego_labels   = vehicle_ego.get_label()
        ego_labels_2 = np.copy(ego_labels)
        ego_labels[ ego_labels==1 ] = 3
        ego_labels_2[ ego_labels==3 ] = 2
        if i < train_size:
            # add org scen
            X_train.append(vehicle_ego.get_poms2())
            X_train_raw.append(vehicle_ego.data_oc[:][::2])
            y_train.append(make_one_hot(ego_labels, num_categ=num_c)[::2])
            # add flipped scen
            if use_flip_augmentation:
                X_train.append(vehicle_ego.get_poms2()[::-1])
                X_train_raw.append(vehicle_ego.data_oc_flip[:][::2][::-1])
                y_train.append(make_one_hot(ego_labels_2, num_categ=num_c)[::-2])
        elif i < train_size + val_size:
            X_valid.append(vehicle_ego.get_poms2())
            X_valid_raw.append(vehicle_ego.data_oc[:][::2])
            y_valid.append(make_one_hot(ego_labels, num_categ=num_c)[::2])
            # add flipped scen
            if use_flip_augmentation:
                X_valid.append(vehicle_ego.get_poms2()[::-1])
                X_valid_raw.append(vehicle_ego.data_oc_flip[:][::2][::-1])
                y_valid.append(make_one_hot(ego_labels_2, num_categ=num_c)[::-2])
        else:
            X_test.append(vehicle_ego.get_poms2())
            X_test_raw.append(vehicle_ego.data_oc[:][::2])
            y_test.append(make_one_hot(ego_labels, num_categ=num_c)[::2])
            # add flipped scen
            if use_flip_augmentation:
                X_test.append(vehicle_ego.get_poms2()[::-1])
                X_test_raw.append(vehicle_ego.data_oc_flip[:][::2][::-1])
                y_test.append(make_one_hot(ego_labels_2, num_categ=num_c)[::-2])

        # Visualize data
        if visualize_data:
            a = vehicle_ego.get_data()
            b = vehicle_other.get_data()
            c = vehicle_other2.get_data()

            plot_pom(vehicle_ego.get_poms2())

            label = ego_labels

            plot_scenario(dt_, [a, b, c])
            plot_labels(label[::2])
        i += 1

#%%
    # ---------------------------------------------- cut-in-out ---------------------------------------------
    i = 0
    print("cut-in-out")
    while(i < size_train):
        # Assign random starting lanes for the vehicles
        random_lanes = np.random.permutation([0, 1, 2])

        man_rand_ego = random_lanes[0]
        man_rand_oth = random_lanes[0]

        # Check if all conditions for this maneuver are met
        # If the ego vehicle is in the middle lane than chose either left or right lane change
        # If it is on the boarder lanes then assign the proper lane change maneuver
        if man_rand_ego == 1:
            maneuver = np.random.choice([1,2])
        elif man_rand_ego == 0:
            maneuver = 1
        else:
            maneuver = 2

        if maneuver == 1:
            lane_d = 1
        else:
            lane_d = -1

        final_lane = random_lanes[0] + lane_d
        man_rand_oth2 = final_lane

        # Assign random starting positions and velocity for other participants
        s_rand_oc  = np.random.randint(22, 45, 2)
        vs_rand_oc = np.random.uniform(23, 30, 2)

        # Assign random starting velocity for ego vehicle
        vs_rand_ego = np.random.uniform(25.0, 28.0)

        # Assign random maneuver start and duration
        act_start = np.random.uniform(4.0, max_duration * 0.35)
        act_dur   = np.random.uniform(act_start_t, act_end_t)

        # Make sure that there is a vehicle in front of the lane after the lane change and position it in front
        s_rand_oc[1] = np.random.randint(20, 40) + ((act_start + 1) * ( vs_rand_ego - vs_rand_oc[1]))

        # Initialize the vehicles
        vehicle_ego    = Vehicle(name='ego', dt=dt_, maneuver=maneuver, s=0, vs=vs_rand_ego, lane=man_rand_ego, lc_duration=act_dur, lc_start=act_start)
        vehicle_other  = Vehicle(name='ov1', dt=dt_, maneuver=0, s=s_rand_oc[0], vs=vs_rand_oc[0], lane=man_rand_oth)
        vehicle_other2 = Vehicle(name='ov2', dt=dt_, maneuver=0, s=s_rand_oc[1], vs=vs_rand_oc[1], lane=man_rand_oth2)

        # Run the simulation
        for _ in range(num_samples):
            vehicles = (vehicle_ego, vehicle_other, vehicle_other2)
            vehicle_ego.check_sensor(vehicles)
            vehicle_ego.update()
            vehicle_other.check_sensor(vehicles)
            vehicle_other.update()
            vehicle_other2.check_sensor(vehicles)
            vehicle_other2.update()

        # Fill the training, validation and test dataset
        ego_labels   = vehicle_ego.get_label()
        ego_labels[ ego_labels==1 ] = 4
        if i < train_size:
            # add org scen
            X_train.append(vehicle_ego.get_poms2())
            X_train_raw.append(vehicle_ego.data_oc[:][::2])
            y_train.append(make_one_hot(ego_labels, num_categ=num_c)[::2])
            # add flipped scen
            if use_flip_augmentation:
                X_train.append(vehicle_ego.get_poms2()[::-1])
                X_train_raw.append(vehicle_ego.data_oc_flip[:][::2][::-1])
                y_train.append(make_one_hot(ego_labels, num_categ=num_c)[::-2])
        elif i < train_size + val_size:
            X_valid.append(vehicle_ego.get_poms2())
            X_valid_raw.append(vehicle_ego.data_oc[:][::2])
            y_valid.append(make_one_hot(ego_labels, num_categ=num_c)[::2])
            # add flipped scen
            if use_flip_augmentation:
                X_valid.append(vehicle_ego.get_poms2()[::-1])
                X_valid_raw.append(vehicle_ego.data_oc_flip[:][::2][::-1])
                y_valid.append(make_one_hot(ego_labels, num_categ=num_c)[::-2])
        else:
            X_test.append(vehicle_ego.get_poms2())
            X_test_raw.append(vehicle_ego.data_oc[:][::2])
            y_test.append(make_one_hot(ego_labels, num_categ=num_c)[::2])
            # add flipped scen
            if use_flip_augmentation:
                X_test.append(vehicle_ego.get_poms2()[::-1])
                X_test_raw.append(vehicle_ego.data_oc_flip[:][::2][::-1])
                y_test.append(make_one_hot(ego_labels, num_categ=num_c)[::-2])

        # Visualize data
        if visualize_data:
            a = vehicle_ego.get_data()
            b = vehicle_other.get_data()
            c = vehicle_other2.get_data()

            plot_pom(vehicle_ego.get_poms2())

            label = ego_labels
            plot_scenario(dt_, [a, b, c])
            plot_labels(label[::2])
        i += 1

#%%
    # ---------------------------------------------- ta-free-cut-out ---------------------------------------------
    i = 0
    print("ta-free-cut-out")
    while(i < size_train):
        # Assign random starting lanes for the vehicles
        lanes = [0, 1, 2]
        random_lane_oth2 = np.random.choice([0, 2])
        lanes.pop(random_lane_oth2)

        # Use remaining lanes for the free lane cut out
        random_lanes_eo = np.random.permutation(lanes)

        man_rand_ego = random_lanes_eo[1]
        man_rand_oth = random_lanes_eo[1]
        man_rand_oth2 = random_lane_oth2

        # Assign the proper lane change maneuver
        if random_lanes_eo[1] - random_lanes_eo[0] == -1:
            maneuver = 1
        else:
            maneuver = 2

        # Assign random starting positions and velocity for other participants
        s_rand_oc  = np.random.randint(0, 30, 2)
        vs_rand_oc = np.random.uniform(24, 29.5, 2)

        # Assign random starting velocity for ego vehicle
        vs_rand_ego = np.random.uniform(27.0, 29.0)

        # Assign random maneuver start and duration
        act_start = np.random.uniform(4.0, max_duration * 0.35)
        act_dur   = np.random.uniform(act_start_t, act_end_t)

        s_rand_oc[0] = np.random.randint(30, 45)

        # Initialize the vehicles
        vehicle_ego    = Vehicle(name='ego', dt=dt_, maneuver=0, s=0, vs=vs_rand_ego, lane=man_rand_ego, lc_duration=act_dur, lc_start=act_start)
        vehicle_other  = Vehicle(name='ov1', dt=dt_, maneuver=maneuver, s=s_rand_oc[0], vs=vs_rand_oc[0], lane=man_rand_oth, lc_duration=act_dur, lc_start=act_start)
        vehicle_other2 = Vehicle(name='ov2', dt=dt_, maneuver=0, s=s_rand_oc[1], vs=vs_rand_oc[1], lane=man_rand_oth2)

        # Run the simulation
        for _ in range(num_samples):
            vehicles = (vehicle_ego, vehicle_other, vehicle_other2)
            vehicle_ego.check_sensor(vehicles)
            vehicle_ego.update()
            vehicle_other.check_sensor(vehicles)
            vehicle_other.update()
            vehicle_other2.check_sensor(vehicles)
            vehicle_other2.update()

        # Fill the training, validation and test dataset
        ego_labels = vehicle_ego.get_label()
        ego_labels_2 = np.copy(ego_labels)
        oth_labels = vehicle_other.get_label()
        oth_labels_2 = np.copy(oth_labels)
        oth_labels[ oth_labels==1 ] = 5
        oth_labels_2[ oth_labels==5 ] = 6
        idx = oth_labels != 0
        ego_labels[idx] = oth_labels[idx]
        ego_labels_2[idx] = oth_labels_2[idx]

        if i < train_size:
            # add org scen
            X_train.append(vehicle_ego.get_poms2())
            X_train_raw.append(vehicle_ego.data_oc[:][::2])
            y_train.append(make_one_hot(ego_labels, num_categ=num_c)[::2])
            # add flipped scen
            if use_flip_augmentation:
                X_train.append(vehicle_ego.get_poms2()[::-1])
                X_train_raw.append(vehicle_ego.data_oc_flip[:][::2][::-1])
                y_train.append(make_one_hot(ego_labels_2, num_categ=num_c)[::-2])
        elif i < train_size + val_size:
            X_valid.append(vehicle_ego.get_poms2())
            X_valid_raw.append(vehicle_ego.data_oc[:][::2])
            y_valid.append(make_one_hot(ego_labels, num_categ=num_c)[::2])
            # add flipped scen
            if use_flip_augmentation:
                X_valid.append(vehicle_ego.get_poms2()[::-1])
                X_valid_raw.append(vehicle_ego.data_oc_flip[:][::2][::-1])
                y_valid.append(make_one_hot(ego_labels_2, num_categ=num_c)[::-2])
        else:
            X_test.append(vehicle_ego.get_poms2())
            X_test_raw.append(vehicle_ego.data_oc[:][::2])
            y_test.append(make_one_hot(ego_labels, num_categ=num_c)[::2])
            # add flipped scen
            if use_flip_augmentation:
                X_test.append(vehicle_ego.get_poms2()[::-1])
                X_test_raw.append(vehicle_ego.data_oc_flip[:][::2][::-1])
                y_test.append(make_one_hot(ego_labels_2, num_categ=num_c)[::-2])

        # Visualize data
        if visualize_data:
            a = vehicle_ego.get_data()
            b = vehicle_other.get_data()
            c = vehicle_other2.get_data()

            plot_pom(vehicle_ego.get_poms2())

            label = ego_labels
            label = oth_labels

            plot_scenario(dt_, [a, b, c])
            plot_labels(label[::2])

        i += 1
    #%%
    # ---------------------------------------------- ta-free-cut-in ----------------------------------------------
    i = 0
    print("ta-free-cut-in")
    while(i < size_train):
        # Assign random starting lanes for the vehicles
        lanes = [0, 1, 2]
        random_lane_oth2 = np.random.choice([0, 2])
        lanes.pop(random_lane_oth2)

        # Use remaining lanes for the free lane cut out
        random_lanes_eo = np.random.permutation(lanes)

        man_rand_ego = random_lanes_eo[0]
        man_rand_oth = random_lanes_eo[1]
        man_rand_oth2 = random_lane_oth2

        # Assign the proper lane change maneuver
        if random_lanes_eo[1] - random_lanes_eo[0] == -1:
            maneuver = 1
        else:
            maneuver = 2

        # Assign random starting positions and velocity for other participants
        s_rand_oc  = np.random.randint(0, 30, 2)
        vs_rand_oc = np.random.uniform(26, 29, 2)

        # Assign random starting velocity for ego vehicle
        vs_rand_ego =np.random.uniform(26.5, 28.0)

        # Assign random maneuver start and duration
        act_start = np.random.uniform(4.0, max_duration * 0.35)
        act_dur   = np.random.uniform(act_start_t, act_end_t)

        s_rand_oc[0] = np.random.randint(35, 45)

        # Initialize the vehicles
        vehicle_ego    = Vehicle(name='ego', dt=dt_, maneuver=0, s=0, lane=man_rand_ego, vs=vs_rand_ego, lc_duration=act_dur, lc_start=act_start)
        vehicle_other  = Vehicle(name='ov1', dt=dt_, maneuver=maneuver, s=s_rand_oc[0], vs=vs_rand_oc[0], lane=man_rand_oth, lc_duration=act_dur, lc_start=act_start)
        vehicle_other2 = Vehicle(name='ov2', dt=dt_, maneuver=0, s=s_rand_oc[1], vs=vs_rand_oc[1], lane=man_rand_oth2)

        # Run the simulation
        for _ in range(num_samples):
            vehicles = (vehicle_ego, vehicle_other, vehicle_other2)
            vehicle_ego.check_sensor(vehicles)
            vehicle_ego.update()
            vehicle_other.check_sensor(vehicles)
            vehicle_other.update()
            vehicle_other2.check_sensor(vehicles)
            vehicle_other2.update()

        # Fill the training, validation and test dataset
        ego_labels = vehicle_ego.get_label()
        ego_labels_2 = np.copy(ego_labels)
        oth_labels = vehicle_other.get_label()
        oth_labels_2 = np.copy(oth_labels)
        oth_labels[ oth_labels==1 ] = 6
        oth_labels_2[ oth_labels==6 ] = 5
        idx = oth_labels != 0
        ego_labels[idx] = oth_labels[idx]
        ego_labels_2[idx] = oth_labels_2[idx]

        if i < train_size:
            # add org scen
            X_train.append(vehicle_ego.get_poms2())
            X_train_raw.append(vehicle_ego.data_oc[:][::2])
            y_train.append(make_one_hot(ego_labels, num_categ=num_c)[::2])
            # add flipped scen
            if use_flip_augmentation:
                X_train.append(vehicle_ego.get_poms2()[::-1])
                X_train_raw.append(vehicle_ego.data_oc_flip[:][::2][::-1])
                y_train.append(make_one_hot(ego_labels_2, num_categ=num_c)[::-2])
        elif i < train_size + val_size:
            X_valid.append(vehicle_ego.get_poms2())
            X_valid_raw.append(vehicle_ego.data_oc[:][::2])
            y_valid.append(make_one_hot(ego_labels, num_categ=num_c)[::2])
            # add flipped scen
            if use_flip_augmentation:
                X_valid.append(vehicle_ego.get_poms2()[::-1])
                X_valid_raw.append(vehicle_ego.data_oc_flip[:][::2][::-1])
                y_valid.append(make_one_hot(ego_labels_2, num_categ=num_c)[::-2])
        else:
            X_test.append(vehicle_ego.get_poms2())
            X_test_raw.append(vehicle_ego.data_oc[:][::2])
            y_test.append(make_one_hot(ego_labels, num_categ=num_c)[::2])
            # add flipped scen
            if use_flip_augmentation:
                X_test.append(vehicle_ego.get_poms2()[::-1])
                X_test_raw.append(vehicle_ego.data_oc_flip[:][::2][::-1])
                y_test.append(make_one_hot(ego_labels_2, num_categ=num_c)[::-2])

        # Visualize data
        if visualize_data:
            a = vehicle_ego.get_data()
            b = vehicle_other.get_data()
            c = vehicle_other2.get_data()

            plot_pom(vehicle_ego.get_poms2())

            label = ego_labels
            label = oth_labels

            plot_scenario(dt_, [a, b, c])
            plot_labels(label)
        i += 1

# %%
    # ---------------------------------------------- ta-sw-cut-in ------------------------------------------------
    i = 0
    print("ta-sw-cut-in")
    while(i < size_train):
        # Assign random starting lanes for the vehicles
        lanes = [0, 1, 2]
        random_lane_oth2 = np.random.choice([0, 2])
        lanes.pop(random_lane_oth2)

        # Use remaining lanes for the free lane cut out
        random_lanes_eo = np.random.permutation(lanes)

        man_rand_ego = random_lanes_eo[0]
        man_rand_oth = random_lanes_eo[1]
        man_rand_oth2 = random_lane_oth2
        man_rand_oth3 = random_lanes_eo[0]

        # Assign the proper lane change maneuver
        if random_lanes_eo[1] - random_lanes_eo[0] == -1:
            maneuver = 1
        else:
            maneuver = 2

        # Assign random starting positions and velocity for other participants
        s_rand_oc  = np.random.randint(0, 30, 3)
        vs_rand_oc = np.random.uniform(26.75, 29, 3)

        # Assign random starting velocity for ego vehicle
        vs_rand_ego = np.random.uniform(25.0, 27.0)

        # Assign random maneuver start and duration
        act_start = np.random.uniform(4.0, max_duration * 0.35)
        act_dur   = np.random.uniform(act_start_t, act_end_t)

        s_rand_oc[0] = np.random.randint(25, 30) - (act_start + 1) * (vs_rand_oc[0] - vs_rand_ego)
        s_rand_oc[2] = np.random.uniform(20, 35) + s_rand_oc[0]  + (act_start + 1) * (vs_rand_oc[0] - vs_rand_oc[1])
        vs_rand_oc[0] = vs_rand_oc[2] + np.random.uniform(0, 1)

        # Initialize the vehicles
        vehicle_ego    = Vehicle(name='ego', dt=dt_, maneuver=0, s=0, lane=man_rand_ego, vs=vs_rand_ego, lc_duration=act_dur, lc_start=act_start)
        vehicle_other  = Vehicle(name='ov1', dt=dt_, maneuver=maneuver, s=s_rand_oc[0], vs=vs_rand_oc[0], lane=man_rand_oth, lc_duration=act_dur, lc_start=act_start)
        vehicle_other2 = Vehicle(name='ov2', dt=dt_, maneuver=0, s=s_rand_oc[1], vs=vs_rand_oc[1], lane=man_rand_oth2)
        vehicle_other3 = Vehicle(name='ov3', dt=dt_, maneuver=0, s=s_rand_oc[2], vs=vs_rand_oc[2], lane=man_rand_oth3)

        # Run the simulation
        for _ in range(num_samples):
            vehicles = (vehicle_ego, vehicle_other, vehicle_other2, vehicle_other3)
            vehicle_ego.check_sensor(vehicles)
            vehicle_ego.update()
            vehicle_other.check_sensor(vehicles)
            vehicle_other.update()
            vehicle_other2.check_sensor(vehicles)
            vehicle_other2.update()
            vehicle_other3.check_sensor(vehicles)
            vehicle_other3.update()

        # Fill the training, validation and test dataset
        ego_labels = vehicle_ego.get_label()
        ego_labels_2 = np.copy(ego_labels)
        oth_labels = vehicle_other.get_label()
        oth_labels_2 = np.copy(oth_labels)
        oth_labels[ oth_labels==1 ] = 7
        oth_labels_2[ oth_labels==7 ] = 8
        idx = oth_labels != 0
        ego_labels[idx] = oth_labels[idx]
        ego_labels_2[idx] = oth_labels_2[idx]

        if i < train_size:
            # add org scen
            X_train.append(vehicle_ego.get_poms2())
            X_train_raw.append(vehicle_ego.data_oc[:][::2])
            y_train.append(make_one_hot(ego_labels, num_categ=num_c)[::2])
            # add flipped scen
            if use_flip_augmentation:
                X_train.append(vehicle_ego.get_poms2()[::-1])
                X_train_raw.append(vehicle_ego.data_oc_flip[:][::2][::-1])
                y_train.append(make_one_hot(ego_labels_2, num_categ=num_c)[::-2])
        elif i < train_size + val_size:
            X_valid.append(vehicle_ego.get_poms2())
            X_valid_raw.append(vehicle_ego.data_oc[:][::2])
            y_valid.append(make_one_hot(ego_labels, num_categ=num_c)[::2])
            # add flipped scen
            if use_flip_augmentation:
                X_valid.append(vehicle_ego.get_poms2()[::-1])
                X_valid_raw.append(vehicle_ego.data_oc_flip[:][::2][::-1])
                y_valid.append(make_one_hot(ego_labels_2, num_categ=num_c)[::-2])
        else:
            X_test.append(vehicle_ego.get_poms2())
            X_test_raw.append(vehicle_ego.data_oc[:][::2])
            y_test.append(make_one_hot(ego_labels, num_categ=num_c)[::2])
            # add flipped scen
            if use_flip_augmentation:
                X_test.append(vehicle_ego.get_poms2()[::-1])
                X_test_raw.append(vehicle_ego.data_oc_flip[:][::2][::-1])
                y_test.append(make_one_hot(ego_labels_2, num_categ=num_c)[::-2])

        # Visualize data
        if visualize_data:
            a = vehicle_ego.get_data()
            b = vehicle_other.get_data()
            c = vehicle_other2.get_data()
            d = vehicle_other3.get_data()

            plot_pom(vehicle_ego.get_poms2())

            label = ego_labels
            label = oth_labels

            plot_scenario(dt_, [a, b, c, d])
            plot_labels(label[::2])

        i += 1

# %%
    # ---------------------------------------------- ta-sw-cut-out -----------------------------------------------
    i = 0
    print("ta-sw-cut-out")
    while(i < size_train):
        # Assign random starting lanes for the vehicles
        lanes = [0, 1, 2]
        random_lane_oth2 = np.random.choice([0, 2])
        lanes.pop(random_lane_oth2)

        # Use remaining lanes for the free lane cut out
        random_lanes_eo = np.random.permutation(lanes)

        man_rand_ego = random_lanes_eo[1]
        man_rand_oth = random_lanes_eo[1]
        man_rand_oth2 = random_lane_oth2
        man_rand_oth3 = random_lanes_eo[1]

        # Assign the proper lane change maneuver
        if random_lanes_eo[1] - random_lanes_eo[0] == -1:
            maneuver = 1
        else:
            maneuver = 2

        # Assign random starting positions and velocity for other participants
        s_rand_oc  = np.random.randint(0, 30, 3)
        vs_rand_oc = np.random.uniform(27, 29.5, 3)

        # Assign random starting velocity for ego vehicle
        vs_rand_ego = np.random.uniform(27, 28.0)

        # Assign random maneuver start and duration
        act_start = np.random.uniform(4.0, max_duration * 0.3)
        act_dur   = np.random.uniform(act_start_t, act_end_t)

        s_rand_oc[0] = np.random.uniform(30, 35)
        s_rand_oc[2] = s_rand_oc[0] + np.random.uniform(10, 15) #+ (act_start + 1) * vs_rand_oc[0]
        vs_rand_oc[2] = vs_rand_oc[0] + np.random.uniform(0, 1)

        # Initialize the vehicles
        vehicle_ego    = Vehicle(name='ego', dt=dt_, maneuver=0, s=0, lane=man_rand_ego, vs=vs_rand_ego, lc_duration=act_dur, lc_start=act_start)
        vehicle_other  = Vehicle(name='ov1', dt=dt_, maneuver=maneuver, s=s_rand_oc[0], vs=vs_rand_oc[0], lane=man_rand_oth, lc_duration=act_dur, lc_start=act_start)
        vehicle_other2 = Vehicle(name='ov2', dt=dt_, maneuver=0, s=s_rand_oc[1], vs=vs_rand_oc[1], lane=man_rand_oth2)
        vehicle_other3 = Vehicle(name='ov3', dt=dt_, maneuver=0, s=s_rand_oc[2], vs=vs_rand_oc[2], lane=man_rand_oth3)

        # Run the simulation
        for _ in range(num_samples):
            vehicles = (vehicle_ego, vehicle_other, vehicle_other2, vehicle_other3)
            vehicle_ego.check_sensor(vehicles)
            vehicle_ego.update()
            vehicle_other.check_sensor(vehicles)
            vehicle_other.update()
            vehicle_other2.check_sensor(vehicles)
            vehicle_other2.update()
            vehicle_other3.check_sensor(vehicles)
            vehicle_other3.update()


        # Fill the training, validation and test dataset
        ego_labels = vehicle_ego.get_label()
        ego_labels_2 = np.copy(ego_labels)
        oth_labels = vehicle_other.get_label()
        oth_labels_2 = np.copy(oth_labels)
        oth_labels[ oth_labels==1 ] = 8
        oth_labels_2[ oth_labels==8 ] = 7
        idx = oth_labels != 0
        ego_labels[idx] = oth_labels[idx]
        ego_labels_2[idx] = oth_labels_2[idx]

        if i < train_size:
            # add org scen
            X_train.append(vehicle_ego.get_poms2())
            X_train_raw.append(vehicle_ego.data_oc[:][::2])
            y_train.append(make_one_hot(ego_labels, num_categ=num_c)[::2])
            # add flipped scen
            if use_flip_augmentation:
                X_train.append(vehicle_ego.get_poms2()[::-1])
                X_train_raw.append(vehicle_ego.data_oc_flip[:][::2][::-1])
                y_train.append(make_one_hot(ego_labels_2, num_categ=num_c)[::-2])
        elif i < train_size + val_size:
            X_valid.append(vehicle_ego.get_poms2())
            X_valid_raw.append(vehicle_ego.data_oc[:][::2])
            y_valid.append(make_one_hot(ego_labels, num_categ=num_c)[::2])
            # add flipped scen
            if use_flip_augmentation:
                X_valid.append(vehicle_ego.get_poms2()[::-1])
                X_valid_raw.append(vehicle_ego.data_oc_flip[:][::2][::-1])
                y_valid.append(make_one_hot(ego_labels_2, num_categ=num_c)[::-2])
        else:
            X_test.append(vehicle_ego.get_poms2())
            X_test_raw.append(vehicle_ego.data_oc[:][::2])
            y_test.append(make_one_hot(ego_labels, num_categ=num_c)[::2])
            # add flipped scen
            if use_flip_augmentation:
                X_test.append(vehicle_ego.get_poms2()[::-1])
                X_test_raw.append(vehicle_ego.data_oc_flip[:][::2][::-1])
                y_test.append(make_one_hot(ego_labels_2, num_categ=num_c)[::-2])

        # Visualize data
        if visualize_data:
            a = vehicle_ego.get_data()
            b = vehicle_other.get_data()
            c = vehicle_other2.get_data()
            d = vehicle_other3.get_data()

            plot_pom(vehicle_ego.get_poms2())

            label = ego_labels
            label = oth_labels

            plot_scenario(dt_, [a, b, c, d])
            plot_labels(label[::2])
        i += 1

#%%
    X_train = np.expand_dims(np.array(X_train, dtype=np.float32), axis=3)
    X_valid = np.expand_dims(np.array(X_valid, dtype=np.float32), axis=3)
    X_test = np.expand_dims(np.array(X_test, dtype=np.float32), axis=3)

    y_train = np.array(y_train, dtype=np.float32)
    y_valid = np.array(y_valid, dtype=np.float32)
    y_test  = np.array(y_test, dtype=np.float32)

    return X_train_raw, X_test_raw, X_valid_raw, X_train, X_test, X_valid, y_train, y_test, y_valid


def generate_batch_for_training(X_train, y_train, batch_size=128, num_steps=15, stride=1):
    while(1):
        X_train, y_train = shuffle(X_train, y_train)
        current_batch_size = 0
        scenario_counter = 0
        x, y = [],[]
        while(current_batch_size < batch_size):
            scenario_len = X_train.shape[1]
            num_windows = (scenario_len - num_steps) // stride
            for i in range(num_windows + 1):
                offset = i * stride
                x.append(X_train[scenario_counter, offset: offset + num_steps,:])
                y.append(y_train[scenario_counter, offset: offset + stride,   :])
                current_batch_size += 1
                if(current_batch_size >= batch_size):
                    break
            scenario_counter += 1

        x, y = np.array(x), np.array(y)
        x, y = shuffle(x, y)
        yield (x, y)


def generate_batch_for_training_og(X_train, y_train, batch_size=128, num_steps=15, stride=1, type='hig'):
    og = OG(h=60, w=20)
    if  type == 'hig':
        convert_to_og = og.construct_hig
    elif type == 'sveg':
        convert_to_og = og.construct_sveg
    elif type == 'fveg':
        convert_to_og = og.construct_fveg

    while(1):
        current_batch_size = 0
        scenario_counter = np.random.randint(len(X_train))
        x, y = [],[]
        while(current_batch_size < batch_size):
            scenario_len = len(X_train[0])
            num_windows = (scenario_len - num_steps) // stride
            for i in range(num_windows + 1):
                offset = i * stride
                x.append(convert_to_og(X_train[scenario_counter][offset: offset + num_steps]))
                y.append(y_train[scenario_counter, offset: offset + stride,   :])
                current_batch_size += 1
                if(current_batch_size >= batch_size):
                    break
            scenario_counter = np.random.randint(len(X_train))

        x, y = np.array(x), np.array(y)
        x, y = shuffle(x, y)
        yield (x, y)


def generate_batch_for_evaluation_og(X_train, y_train, batch_size=1, num_steps=15, stride=1, type='hig'):
    og = OG(h=60, w=20)
    if  type == 'hig':
        convert_to_og = og.construct_hig
    elif type == 'sveg':
        convert_to_og = og.construct_sveg
    elif type == 'fveg':
        convert_to_og = og.construct_fveg

    scenario_counter = 0
    scenario_num = len(X_train)
    while scenario_counter < scenario_num:
        current_batch_size = 0
        x, y = [],[]
        while current_batch_size < batch_size and scenario_counter < scenario_num:
            scenario_len = len(X_train[0])
            num_windows = (scenario_len - num_steps) // stride
            for i in range(num_windows + 1):
                offset = i * stride
                x.append(convert_to_og(X_train[scenario_counter][offset: offset + num_steps]))
                y.append(y_train[scenario_counter, offset: offset + stride,   :])
                current_batch_size += 1
                if(current_batch_size >= batch_size):
                    break
            scenario_counter += 1
        x, y = np.array(x), np.array(y)
        yield (x, y)


def generate_batch_for_evaluation(X_train, y_train, batch_size=1, num_steps=15, stride=1):
    scenario_counter = 0
    scenario_num = len(X_train)
    while scenario_counter < scenario_num:
        current_batch_size = 0
        x, y = [],[]
        while current_batch_size < batch_size and scenario_counter < scenario_num:
            scenario_len = X_train.shape[1]
            num_windows = (scenario_len - num_steps) // stride
            for i in range(num_windows + 1):
                offset = i * stride
                x.append(X_train[scenario_counter, offset: offset + num_steps,:])
                y.append(y_train[scenario_counter, offset: offset + stride,   :])
                current_batch_size += 1
                if(current_batch_size >= batch_size):
                    break
            scenario_counter += 1
        x, y = np.array(x), np.array(y)
        yield (x, y)


def generate_windows_from_real_data(X_real, num_steps=15, stride=1):
    scenario_len = X_real.shape[1]
    num_windows = (scenario_len - num_steps) // stride
    for i in range(num_windows + 1):
        offset = i * stride
        yield X_real[:, offset: offset + num_steps,:]


def generate_windows_from_real_data_fveg(X_real, num_steps=15, stride=1):
    scenario_len = X_real.shape[3]
    num_steps *= 3
    num_windows = (scenario_len - num_steps) // stride
    for i in range(num_windows + 1):
        offset = i * stride
        yield X_real[:, :, :, offset: offset + num_steps]


def generate_windows_from_real_data_og(X_real):
    for x in X_real:
        yield np.expand_dims(x, axis=0)
