Code for reproducing the results of the paper:
#### Polar Occupancy Map - A Compact Traffic Representation for Deep Learning Scenario Classification

---
Please install the necessary dependencies before running the code.
- numpy
- tensorflow
- matplotlib
- sklearn
- cv2

---
The data used in the paper is stored in the ```training``` folder. The compressed archive contains the training data and real world data used for validation.
If you would like to generate new syntactic data you can use ```generate_data.py```. To change the behavior please use the following flags:
``` python
data_size            = 6000                     # Number of scenarios that will be generated
data_file_name       = 'training/data-new.npy'  # Name of the new file
show_random_scenario = True
show_all_scenarios   = False
```

---
To run the training and validation on the Polar Occupancy Map (POM), History Grid, Stacked Velocity Grid or Full Velocity Grid, use their respective file ```python compact_scenarios_{pom, hig, sveg, fveg}.py```

To change the behavior, use the following flags:
```python
data_file_name       = 'training/data-paper.npy'
show_random_scenario = True
show_all_scenarios   = False

load_model           = True
model_files          = './models/pom-50-m/model.ckpt' # only the POM model is saved to reduce repository size
train_model          = False
calc_valid_acc       = True  # Calculate the ROC and AUC for the model on the validation data
save_model           = False # When training use this to save the model

num_steps   = 50  # Number of POM/VeG representations that will be used as input for the network
stride      = 1   # Stride for the sliding window
offset      = (num_steps // 2) - 1 # The network is making prediction in the middle of the window
num_classes = 9

```
Note that at least 16GB of RAM is necessary to run the training with the default batch size. During training a report will be created after a certain number of steps defined with the ```display_step``` variable.

---
