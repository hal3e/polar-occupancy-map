# Compute ROC curve and ROC AUC for each class
# Code based on: http://benalexkeen.com/scoring-classifier-models-using-scikit-learn/

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from sklearn.metrics import roc_curve, auc

def plot_roc_auc(y_val, y_val_c, n_classes=10, show_zoom=True, latex_style=True, fsize=(9, 5)):
    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    all_y_val_i = np.array([])
    all_y_val_c = np.array([])

    for i in range(n_classes):
        if i == 0:
            all_y_val_i = y_val[:, i]
            all_y_val_c = y_val_c[:, i]
        else:
            all_y_val_i = np.concatenate([all_y_val_i, y_val[:, i]])
            all_y_val_c = np.concatenate([all_y_val_c, y_val_c[:, i]])
        fpr[i], tpr[i], _ = roc_curve(y_val[:, i], y_val_c[:, i], pos_label=1.0)
        roc_auc[i] = auc(fpr[i], tpr[i])

    # Compute micro-average ROC curve and ROC area
    fpr["average"], tpr["average"], _ = roc_curve(all_y_val_i, all_y_val_c, pos_label=1.0)
    roc_auc["average"] = auc(fpr["average"], tpr["average"])

    if latex_style:
        plt.rcParams['text.latex.preamble']=[r"\usepackage{lmodern}"]
        #Options
        params = {'text.usetex' : True,
                  'font.size' : 16,
                  # 'font.family' : 'lmodern',
                  'font.family' : 'cmr10',
                  'text.latex.unicode': True,
                  }
        plt.rcParams.update(params)

    f  = plt.figure(figsize=fsize)
    ax = plt.subplot(111)

    ax.plot(fpr["average"], tpr["average"], label='ROC A (AUC={0:0.3f})'.format(roc_auc["average"]), color='deepskyblue', linestyle=':', linewidth=3)

    if show_zoom:
        axins = zoomed_inset_axes(ax, 4.5, loc=10) # zoom-factor: 2.5, location: upper-left
        axins.plot(fpr["average"], tpr["average"], label='ROC A (AUC={0:0.3f})'.format(roc_auc["average"]), color='deepskyblue', linestyle=':', linewidth=3)

    # Plot each individual ROC curve
    for i in range(n_classes):
        ax.plot(fpr[i], tpr[i], lw=2, label='ROC {0} (AUC={1:0.3f})'.format(i+1, roc_auc[i]))
        if show_zoom:
            axins.plot(fpr[i], tpr[i], lw=2, label='ROC {0} (AUC={1:0.3f})'.format(i, roc_auc[i]))

    if show_zoom:
        axins.set_xlim([0.0, 0.1])
        axins.set_ylim([0.85, 1.0])

    ax.plot([0, 1], [0, 1], 'k--', lw=1)
    # ax.set_xlim([0.0, 1.0])
    ax.set_xlim([0.0, 0.4])
    ax.set_ylim([0.4, 1.05])
    ax.set_xlabel('False Positive Rate')
    ax.set_ylabel('True Positive Rate')
    ax.legend(loc="lower right", prop={'size': 13})

    plt.show()
    f.savefig("figures/roc.pdf")
    f.savefig("figures/roc.svg")
