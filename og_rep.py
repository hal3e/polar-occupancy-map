# Occupany Grid representations
import numpy as np

class OG:
    def __init__(self, h=60, w=20, use_clip=True):
        # create placeholders for the reprezentation
        self.h    = h
        self.w    = w
        self.w_h  = int(w/2.0)

        self.clip = np.pi
        if use_clip:
            self.clip = 45 * (np.pi / 180.0)

    def construct_veg(self, env):
        ocg = np.zeros((self.h, self.w, 3))
        # env is a list of timesteps containing the vehicles locations and velocity
        # it is ordered as: ego_s, ego_d, ego_vs, ego_vd, vehicles_ds, veicles_dv, vehicles_vs, vehicles_vd ...
        ocg[40, self.w_h, 0] = 1.0
        ocg[40, self.w_h, 1] = 0
        ocg[40, self.w_h, 2] = (env[3] / 10.0)

        # check wether we have other vehicles inside the env
        num_elements = len(env) // 4
        if  num_elements > 1:
            # get the distance to the vehicles
            for j in range(num_elements - 1):
                    s  = env[4 + 4 * j]
                    d  = env[5 + 4 * j]

                    # check wether it is in the bounds and if sensor clip
                    angl = np.arctan2(d, s)
                    if s >-40 and s <=80  and d >-10.0 and d <=10.0 and angl >= -self.clip and angl <= self.clip:
                        # nomralize vs and vd [-1, 1] for [-10, 10] m/s
                        vs = (env[6 + 4 * j] / 10.0)
                        vd = (env[7 + 4 * j] / 10.0)

                        ocg[int(40 - s/2.0), int(self.w_h - d), 0] = 1.0
                        ocg[int(40 - s/2.0), int(self.w_h - d), 1] = vs
                        ocg[int(40 - s/2.0), int(self.w_h - d), 2] = vd

        return ocg

    def construct_hig(self, envs):
        hig = np.zeros((self.h, self.w, 3))
        len_env = len(envs)

        for i,env in enumerate(envs):
            ocg = self.construct_veg(env)
            hig[:,:,0]  = np.maximum(hig[:,:,0], ocg[:,:,0] * ((i/len_env)))
            hig[:,:,1:] = ocg[:,:,1:]

        return hig

    def construct_sveg(self, envs):
        ocg1 = self.construct_veg(envs[0])
        ocg2 = self.construct_veg(envs[-1])

        return np.dstack((ocg1, ocg2))

    def construct_fveg(self, envs):
        vegs = []
        for i,env in enumerate(envs):
            ocg = self.construct_veg(env)
            vegs.append(ocg)
        return np.dstack(vegs)
