import tensorflow as tf
from util import get_rf

def make_model(num_steps, phi_size, num_classes):

    # kernel sizes
    k  = [3, 3, 3, 3, 3, 3, 3, 3, 3]
    # horizontal strides and dilation
    sh = [1, 2, 1, 2, 1, 2, 1, 2, 2]
    dh = [1, 1, 1, 1, 1, 1, 1, 1, 1]
    # vertical strides and dilation
    sv = [1, 1, 1, 1, 1, 2, 1, 2, 2]
    dv = [1, 1, 1, 1, 2, 1, 4, 1, 1]

    print("--- Building model ---")
    print("horizontal receptive field: ", get_rf(k, sh, dh))
    print("  vertical receptive field: ", get_rf(k, sv, dv))

    # Input and output
    X = tf.placeholder(tf.float32, (None, num_steps, phi_size, 1))
    Y = tf.placeholder(tf.float32, (None, num_classes))

    # Changeable variables
    dropout = tf.placeholder(tf.float32)
    learning_rate = tf.placeholder(tf.float32)

    actvtn = tf.nn.selu
    initilzr   = tf.initializers.variance_scaling(distribution='uniform')
    pad   = 'same'

    # Convolutional filters
    conv0d  = tf.layers.conv2d(inputs=X,       filters=16, kernel_size=3, strides=(sv[0], sh[0]), dilation_rate=(dv[0], dh[0]),
                               padding=pad, activation=actvtn, kernel_initializer=initilzr)
    # conv0r  = X + conv0d      # residual block

    conv0   = tf.layers.conv2d(inputs=conv0d,  filters=32, kernel_size=3, strides=(sv[1], sh[1]), dilation_rate=(dv[1], dh[1]),
                               padding=pad, activation=actvtn, kernel_initializer=initilzr)

    conv1d   = tf.layers.conv2d(inputs=conv0,  filters=32, kernel_size=3, strides=(sv[2], sh[2]), dilation_rate=(dv[2], dh[2]),
                               padding=pad, activation=actvtn, kernel_initializer=initilzr)
    conv1r  = conv0 + conv1d  # residual block

    conv1   = tf.layers.conv2d(inputs=conv1r,  filters=32, kernel_size=3, strides=(sv[3], sh[3]), dilation_rate=(dv[3], dh[3]),
                               padding=pad, activation=actvtn, kernel_initializer=initilzr)

    conv2d   = tf.layers.conv2d(inputs=conv1,  filters=32, kernel_size=3, strides=(sv[4], sh[4]), dilation_rate=(dv[4], dh[4]),
                               padding=pad, activation=actvtn, kernel_initializer=initilzr)

    conv2    = tf.layers.conv2d(inputs=conv2d, filters=64, kernel_size=3, strides=(sv[5], sh[5]), dilation_rate=(dv[5], dh[5]),
                               padding=pad, activation=actvtn, kernel_initializer=initilzr)

    conv3d   = tf.layers.conv2d(inputs=conv2,  filters=64, kernel_size=3, strides=(sv[6], sh[6]), dilation_rate=(dv[6], dh[6]),
                               padding=pad, activation=actvtn, kernel_initializer=initilzr)
    conv3r   = conv2 + conv3d # residual block

    conv3    = tf.layers.conv2d(inputs=conv3r, filters=128, kernel_size=3, strides=(sv[7], sh[7]), dilation_rate=(dv[7], dh[7]),
                               padding=pad, activation=actvtn, kernel_initializer=initilzr)

    conv4    = tf.layers.conv2d(inputs=conv3, filters=128, kernel_size=3, strides=(sv[8], sh[8]), dilation_rate=(dv[8], dh[8]),
                               padding=pad, activation=actvtn, kernel_initializer=initilzr)

    shp_conv4 = conv4.shape

    conv4_f = tf.layers.flatten(conv4)
    conv4_f = tf.contrib.nn.alpha_dropout(conv4_f, dropout)

    dense1  = tf.layers.dense(tf.reshape(conv4_f, [-1, shp_conv4[1] * shp_conv4[2] * 128]), 1024, activation=actvtn, kernel_initializer=initilzr)
    dense1 = tf.contrib.nn.alpha_dropout(dense1, dropout)

    dense2  = tf.layers.dense(tf.reshape(dense1, [-1, 1024]), 512, activation=actvtn, kernel_initializer=initilzr)
    dense2 = tf.contrib.nn.alpha_dropout(dense2, dropout)

    dense3  = tf.layers.dense(tf.reshape(dense2, [-1, 512]), 256, activation=actvtn, kernel_initializer=initilzr)
    # dense3 = tf.contrib.nn.alpha_dropout(dense3, dropout)

    # Output logits, softmax for classes
    # c_logits  = tf.layers.dense(tf.reshape(dense3, [-1, 128]), num_classes, activation=None, kernel_initializer=initilzr)
    c_logits  = tf.layers.dense(tf.reshape(dense3, [-1, 256]), num_classes, activation=None, kernel_initializer=initilzr)
    c_soft = tf.nn.softmax(c_logits)

    cost_sup = tf.reduce_sum(tf.nn.softmax_cross_entropy_with_logits_v2(logits=c_logits, labels=Y))
    train_op_sup = tf.train.AdamOptimizer(learning_rate).minimize(cost_sup)  # construct an optimizer

    print("--- Model built ---")
    return X, Y, train_op_sup, cost_sup, c_soft, dropout, learning_rate


def make_model_vanilla_CNN(num_steps, phi_size, num_classes):

    # kernel sizes
    k  = [3, 3, 3, 3, 3, 3, 3, 3, 3]
    # horizontal strides and dilation
    sh = [1, 2, 1, 2, 1, 2, 1, 2, 2]
    dh = [1, 1, 1, 1, 1, 1, 1, 1, 1]
    # vertical strides and dilation
    sv = [1, 1, 1, 1, 1, 2, 1, 2, 2]
    dv = [1, 1, 1, 1, 1, 1, 1, 1, 1]

    print("--- Building model ---")
    print("horizontal receptive field: ", get_rf(k, sh, dh))
    print("  vertical receptive field: ", get_rf(k, sv, dv))

    # Input and output
    X = tf.placeholder(tf.float32, (None, num_steps, phi_size, 1))
    Y = tf.placeholder(tf.float32, (None, num_classes))

    # Changeable variables
    dropout = tf.placeholder(tf.float32)
    learning_rate = tf.placeholder(tf.float32)

    actvtn = tf.nn.relu
    initilzr   = tf.initializers.variance_scaling(distribution='uniform')
    pad   = 'same'

    # Convolutional filters
    conv0d  = tf.layers.conv2d(inputs=X,       filters=16, kernel_size=3, strides=(sv[0], sh[0]), dilation_rate=(dv[0], dh[0]),
                               padding=pad, activation=actvtn, kernel_initializer=initilzr)
    # conv0r  = X + conv0d      # residual block
    print(conv0d.shape)
    conv0   = tf.layers.conv2d(inputs=conv0d,  filters=32, kernel_size=3, strides=(sv[1], sh[1]), dilation_rate=(dv[1], dh[1]),
                               padding=pad, activation=actvtn, kernel_initializer=initilzr)

    print(conv0.shape)
    conv1d   = tf.layers.conv2d(inputs=conv0,  filters=32, kernel_size=3, strides=(sv[2], sh[2]), dilation_rate=(dv[2], dh[2]),
                               padding=pad, activation=actvtn, kernel_initializer=initilzr)
    conv1r  = conv0 + conv1d  # residual block

    print(conv1r.shape)
    conv1   = tf.layers.conv2d(inputs=conv1r,  filters=32, kernel_size=3, strides=(sv[3], sh[3]), dilation_rate=(dv[3], dh[3]),
                               padding=pad, activation=actvtn, kernel_initializer=initilzr)

    print(conv1.shape)
    conv2d   = tf.layers.conv2d(inputs=conv1,  filters=32, kernel_size=3, strides=(sv[4], sh[4]), dilation_rate=(dv[4], dh[4]),
                               padding=pad, activation=actvtn, kernel_initializer=initilzr)

    print(conv2d.shape)
    conv2    = tf.layers.conv2d(inputs=conv2d, filters=64, kernel_size=3, strides=(sv[5], sh[5]), dilation_rate=(dv[5], dh[5]),
                               padding=pad, activation=actvtn, kernel_initializer=initilzr)

    print(conv2.shape)
    conv3d   = tf.layers.conv2d(inputs=conv2,  filters=64, kernel_size=3, strides=(sv[6], sh[6]), dilation_rate=(dv[6], dh[6]),
                               padding=pad, activation=actvtn, kernel_initializer=initilzr)
    conv3r   = conv2 + conv3d # residual block

    print(conv3r.shape)
    conv3    = tf.layers.conv2d(inputs=conv3r, filters=128, kernel_size=3, strides=(sv[7], sh[7]), dilation_rate=(dv[7], dh[7]),
                               padding=pad, activation=actvtn, kernel_initializer=initilzr)

    print(conv3.shape)
    conv4    = tf.layers.conv2d(inputs=conv3, filters=128, kernel_size=3, strides=(sv[8], sh[8]), dilation_rate=(dv[8], dh[8]),
                               padding=pad, activation=actvtn, kernel_initializer=initilzr)

    print(conv4.shape)
    shp_conv4 = conv4.shape

    conv4_f = tf.layers.flatten(conv4)
    conv4_f = tf.nn.dropout(conv4_f, dropout)

    dense1  = tf.layers.dense(tf.reshape(conv4_f, [-1, shp_conv4[1] * shp_conv4[2] * 128]), 1024, activation=actvtn, kernel_initializer=initilzr)
    dense1 = tf.nn.dropout(dense1, dropout)

    dense2  = tf.layers.dense(tf.reshape(dense1, [-1, 1024]), 512, activation=actvtn, kernel_initializer=initilzr)
    dense2 = tf.nn.dropout(dense2, dropout)

    dense3  = tf.layers.dense(tf.reshape(dense2, [-1, 512]), 256, activation=actvtn, kernel_initializer=initilzr)
    # dense3 = tf.contrib.nn.alpha_dropout(dense3, dropout)

    # Output logits, softmax for classes
    # c_logits  = tf.layers.dense(tf.reshape(dense3, [-1, 128]), num_classes, activation=None, kernel_initializer=initilzr)
    c_logits  = tf.layers.dense(tf.reshape(dense3, [-1, 256]), num_classes, activation=None, kernel_initializer=initilzr)
    c_soft = tf.nn.softmax(c_logits)

    cost_sup = tf.reduce_sum(tf.nn.softmax_cross_entropy_with_logits_v2(logits=c_logits, labels=Y))
    train_op_sup = tf.train.AdamOptimizer(learning_rate).minimize(cost_sup)  # construct an optimizer

    print("--- Model built ---")
    return X, Y, train_op_sup, cost_sup, c_soft, dropout, learning_rate
