import time
import numpy as np
import tensorflow as tf
from og_rep import OG
import matplotlib.pyplot as plt
from roc_auc import plot_roc_auc
from neural_network_model_occupancy_grid import make_model_hig, make_model_sveg, make_model_fveg
from util import plot_highway, plot_scenario, plot_scenario_oc, plot_labels, plot_pom, cut_offset2, make_one_hot
from generate_data_util import get_data, generate_batch_for_training_og, generate_batch_for_evaluation_og, generate_windows_from_real_data_og
# %%
scenario_labels = ['driving_in_lane', 'lane_change', 'freelane_cut_in', 'freelane_cut_out', 'cut-in-out',
                   'ta-free-cut-out', 'ta-free-cut-in', 'ta-sw-cut-in', 'ta-sw-cut-out']

# type can be hig, sveg or fveg
occupancy_grid_type = 'hig'

data_file_name       = 'training/data-paper.npy'
show_random_scenario = True
show_all_scenarios   = False

load_model           = False
model_files          = './models/<MODEL_NAME>.ckpt'
train_model          = True
calc_valid_acc       = True # Calculate the ROC and AUC for the model on the validation data
save_model           = False # When training use this to save the model in the models/new-model folder

num_steps   = 50  # Number of VeG representations that will be used as input for the network
stride      = 1   # Stride for the sliding window
offset      = (num_steps // 2) - 1 # The network is making prediction in the middle of the window
num_classes = 9

with open(data_file_name, 'rb') as f:
    X_train_raw = np.load(f)
    X_test_raw  = np.load(f)
    X_valid_raw = np.load(f)
    _           = np.load(f)
    _           = np.load(f)
    _           = np.load(f)
    y_train     = np.load(f)
    y_test      = np.load(f)
    y_valid     = np.load(f)
print("\n--- Loaded " + data_file_name + " ---")

# cut offset
y_train, y_test, y_valid  =  cut_offset2(y_train, offset), cut_offset2(y_test, offset), cut_offset2(y_valid, offset)

training_size = len(X_train_raw)
test_size = len(X_test_raw)
valid_size = len(X_valid_raw)
print("Training size: {}".format(training_size))
print("Test size: {}".format(test_size))
print("Validation size: {}\n".format(valid_size))
#%%


#%%
# Show random scenario
if show_random_scenario:
    og_temp = OG()
    idx = np.random.choice(len(X_train_raw))
    plot_scenario_oc(0.125, X_train_raw[idx])
    plt.show()
    plt.imshow(og_temp.construct_hig(X_train_raw[idx])[:,:,0])
    print("class: ", scenario_labels[np.max(np.argmax(y_train[idx], axis=1))])
    plt.show()
    plt.plot(np.argmax(y_train[idx], axis=1))
    plt.show()
#%%


# Show all scenarios
if show_all_scenarios:
    for x in X_train:
        plot_scenario_oc(0.125, x)

# %%
# Make the neural network model
tf.reset_default_graph()
X, Y, train_op_sup, cost_sup, c_soft, dropout, learning_rate = make_model_hig(num_classes=num_classes)

config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.95
sess = tf.Session(config=config)
saver = tf.train.Saver()

# Restore a model checkpoint or randomly initialize weights
if load_model:
    saver.restore(sess, model_files)
else:
    sess.run(tf.global_variables_initializer())
# %%

# Train the model ------------------------------------------------------------------------------------------------------------------
if train_model:
    print("--- Running Training ---")
    batch_size     = 128
    training_steps = 256 * 9
    display_step   = 250
    l_rate_        = 0.0001
    dropout_       = 0.9 # keep probab
    mb_loss        = 0
    t_acc          = 0

    # Create generator
    next_batch =  generate_batch_for_training_og(X_train_raw, y_train, batch_size=batch_size, num_steps=num_steps, stride=stride, type=occupancy_grid_type)

    start_time = time.time()
    disp_time  = time.time()
    for step in range(1, training_steps+1):
        batch_x, batch_y = next(next_batch)
        batch_y = batch_y.squeeze()

        _, b_loss, ls_cs = sess.run([train_op_sup, cost_sup, c_soft], feed_dict={X: batch_x, Y: batch_y, dropout: dropout_, learning_rate: l_rate_})
        mb_loss += b_loss / display_step

        b_acc_t = np.sum(np.argmax(ls_cs, axis=1) == np.argmax(batch_y, axis=1)) / batch_size
        t_acc += b_acc_t / display_step
        if step % display_step == 0:
            c_time = time.time()
            print("Step {}, Mean batch Loss= {:.4f}, time: {:.2f}, lrate: {:.2e}".format(step, mb_loss, c_time - disp_time, l_rate_))
            disp_time = c_time
            print("t acc: ", round(t_acc,4))

            # Evaluate accuracy on the test dataset
            b_counter = 0
            b_acc = 0
            batch_size_eval = 256
            next_batch_eval =  generate_batch_for_evaluation_og(X_test_raw, y_test, batch_size=batch_size_eval, num_steps=num_steps, stride=stride, type=occupancy_grid_type)
            for x_e, y_e in next_batch_eval:
                y_e = y_e.squeeze()
                ls_cs = sess.run(c_soft, feed_dict={ X: x_e, Y: y_e, dropout: 1.0})
                b_acc += np.sum(np.argmax(ls_cs, axis=1) == np.argmax(y_e, axis=1)) / batch_size_eval
                b_counter += 1

            print("v acc: ", round(b_acc/b_counter,4))
            mb_loss = 0
            t_acc = 0
        # decay learning rate
        l_rate_ *= 0.99975
    elapsed_time = time.time() - start_time
    print("--- Training finished in: {:.2f}s ---".format(elapsed_time))

    if save_model:
        save_path = saver.save(sess, "./models/new-model-hig-20-m/model.ckpt")
        print("--- Model saved in file: {:s} ---".format(save_path))
# %%
# Get validation accuracy
if calc_valid_acc:
    print("--- Evaluating model on validation data ---")
    b_counter = 0
    b_acc = 0
    batch_size_eval = 256
    next_batch_eval =  generate_batch_for_evaluation_og(X_valid_raw, y_valid, batch_size=batch_size_eval, num_steps=num_steps, stride=stride, type=occupancy_grid_type)
    y_val_c = []
    y_val = []
    for x_e, y_e in next_batch_eval:
        y_e = y_e.squeeze()
        y_val.append(y_e)
        ls_cs = sess.run(c_soft, feed_dict={ X: x_e, Y: y_e, dropout:1.0})
        y_val_c.append(ls_cs)
        sum = np.sum(np.argmax(ls_cs, axis=1) == np.argmax(y_e, axis=1))
        b_acc += np.sum(np.argmax(ls_cs, axis=1) == np.argmax(y_e, axis=1)) / batch_size_eval
        b_counter += 1

    print("accuracy: ", round(b_acc/b_counter,3))

    y_val_c = np.vstack(y_val_c)
    y_val = np.vstack(y_val)

    to_plot_c = np.argmax(y_val_c, axis=1)
    to_plot_y = np.argmax(y_val, axis=1)

    plot_roc_auc(y_val, y_val_c, n_classes=num_classes, show_zoom=False, latex_style=False)

scenario_labels = ['driving_in_lane', 'lane_change', 'freelane_cut_in', 'freelane_cut_out', 'cut-in-out',
                   'tof-free-cut-out', 'tof-free-cut-in', 'tof-sw-cut-in', 'tof-sw-cut-out']
# %%
