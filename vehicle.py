import numpy as np
import cv2

class Vehicle:
    ''' This is the Vehicle model class that is used to generate the data. The vehicle can keep a lane or change the lane if the lane is free.
       The sensor system provides sensor fusion data with the relative position of other participants.'''

    def __init__(self, name, dt, add_noise=False, lane=None, s=0, vs=30, maneuver=0, lc_duration=3.0, lc_start=2.0, debug=False, max_step=100, use_clip=True):
        # Initialize class variables
        self.name = name
        self.debug = debug
        self.lane_width = 3.5
        self.half_lane = self.lane_width / 2.0
        self.lane_pos = [-self.lane_width, 0, self.lane_width]
        self.scale_factor = 1.0
        vehicle_lengths  = [4.4, 6.0, 10.0, 15.0]
        vehicle_widths   = [1.8, 2.0, 2.5, 2.5]
        vehicle_type     = np.random.randint(len(vehicle_lengths), size=1)[0]
        self.cc_distance = np.random.uniform(35, 40)
        self.previous_sd = 0
        self.previous_dd = 0

        self.clip = np.pi
        if use_clip:
            self.clip = 45 * (np.pi / 180.0)


        self.length = vehicle_lengths[vehicle_type] * self.scale_factor
        self.width  = vehicle_widths[vehicle_type]  * self.scale_factor
        self.step   = 0
        self.max_step = max_step

        dl, dw = self.length/2.0, self.width/2.0
        self.base_s = np.array([dl, dl, -dl, -dl])
        self.base_d = np.array([-dw, dw, dw, -dw])

        self.phi = np.linspace(np.pi, -np.pi, 360*8)
        self.pom = np.zeros_like(self.phi)
        self.pom_no_sigma = np.copy(self.pom)
        self.poms = []

        sigma = 0.005
        temp = np.linspace(-1.0, 1.0, 360*8)
        self.pom_gauss  = np.exp(-0.5 * (temp / sigma)**2) * (1.0 / (np.sqrt(sigma * 256)))

        self.label_ = 0
        self.label = []

        self.s_ = s
        self.s = []

        self.vs_ = vs
        self.vs_max = vs
        self.vs = []
        self.decrease_rate = 4.5
        self.increase_rate = 2.5

        self.vd_= 0
        self.vd = []
        self.vd_rate = ( 4.0 * self.lane_width) / (lc_duration * lc_duration)

        self.sensor_s = []
        self.sensor_d = []

        self.maneuvers = ['KL', 'LCL', 'LCR']
        self.maneuver_checked = False
        self.maneuver_start = lc_start
        self.maneuver = 0
        self.sensor_s_val = 0
        self.sensor_d_val = 0
        self.maneuver_start_rand = np.random.uniform(0.0, 0.3)
        self.maneuver_dur_rand   = np.random.uniform(-0.3, 0.0)

        self.data_oc      = []
        self.data_oc_flip = []

        # Assign the proper lane change maneuver
        if maneuver == 1:
            self.maneuver = maneuver
            if lane is None:
                self.lane=np.random.randint(0,2)
            else:
                assert lane < 2, "Can't make lane change from this lane"
                self.lane = lane

        elif maneuver == 2:
            self.maneuver = maneuver
            if lane is None:
                self.lane=np.random.randint(1,3)
            else:
                assert lane > 0, "Can't make lane change from this lane"
                self.lane = lane
        else:
            if lane is None:
                self.lane = np.random.randint(3)
            else:
                self.lane = lane

        self.d_ = self.lane_pos[self.lane]
        self.d = []

        self.lc_duration = lc_duration

        self.t = 0
        self.dt = dt

        self.add_noise = add_noise
        self.noise_std = 0.05
        if self.debug:
            print(self.name + " - man: " + self.maneuvers[self.maneuver])

    # Reset recordings
    def reset(self):
        self.s        = []
        self.d        = []
        self.vs       = []
        self.vd       = []
        self.sensor_s = []
        self.sensor_d = []
        self.data_oc  = []
        self.data_oc_flip  = []

    # Main motion update
    def update(self):
        self.update_vel()
        self.s_ = self.s_ + self.vs_ * self.dt + np.random.uniform(-0.1, 0.1)
        self.d_ = self.d_ + self.vd_ * self.dt + np.random.uniform(-0.035, 0.035)
        self.s.append(self.s_)
        self.d.append(self.d_)
        self.vs.append(self.vs_)
        self.vd.append(self.vd_)
        self.sensor_s.append(self.sensor_s_val)
        self.sensor_d.append(self.sensor_d_val)
        self.t += self.dt
        self.step += 1
        self.sensor_s_val = 0
        self.sensor_d_val = 0

        self.poms.append(np.copy(self.pom))
        self.label.append(self.label_)
        self.label_ = 0

    # Update velocities and execute the maneuver
    def update_vel(self):
        if self.maneuver != 0 and self.t >= self.maneuver_start and self.t <= self.maneuver_start + self.lc_duration:
            if self.t < self.maneuver_start + self.lc_duration / 2.0: # Start lane change by adjusting vd
                if self.maneuver == 1:     # Left lane change
                    self.vd_ += self.vd_rate * self.dt
                elif self.maneuver == 2:   # Rigth lane change
                    self.vd_ -= self.vd_rate * self.dt
            else: # End lane change by reducing vd
                if self.maneuver == 1:
                    self.vd_ -= self.vd_rate * self.dt
                elif self.maneuver == 2:
                    self.vd_ += self.vd_rate * self.dt
            # add appropriate label
            if self.t >= self.maneuver_start + self.maneuver_start_rand and self.t <= self.maneuver_start + self.lc_duration + self.maneuver_dur_rand:
                self.label_ = 1

        elif self.t > self.maneuver_start + self.lc_duration:
            self.maneuver = 0
            self.vd_ = 0.0

    def distance(self, a1, a2, b1, b2):
        return np.sqrt((a1 - a2)**2 + (b1 - b2)**2)

    def distance2(self, a1, a2):
        return np.sqrt(a1**2 + a2**2)

    # Get sensor information from all vehicles and make the PolFV representation
    def check_sensor(self, vehicles):
        vd_noise = np.random.uniform(-0.05, 0.05)
        temp_oc = [self.s_, self.d_, self.vs_, self.vd_ + vd_noise]
        temp_oc_flip = [0, 0, 0, -self.vd_ + vd_noise]

        self.pom.fill(0.0)
        self.pom_no_sigma.fill(0.0)
        k = -0.011
        n = 1.011

        for vehicle in vehicles:
            if vehicle.name != self.name:
                sd, dd = vehicle.s_ - self.s_, vehicle.d_ - self.d_

                # Active cruise control
                if sd  < 35 and sd > 3:
                    if abs(dd) < self.half_lane and self.vs_ >= vehicle.vs_:
                        self.vs_ -= self.decrease_rate * self.dt
                        if self.vs_ < vehicle.vs_: self.vs_ = vehicle.vs_
                    else:
                        self.vs_ += self.increase_rate * self.dt
                        if self.vs_ > self.vs_max: self.vs_ = self.vs_max

                # Calculate the Polar Occupancy Grid
                max_dist = 75
                if self.distance2(sd, dd) < max_dist:
                    # # add data for OC
                    # oc_angle = np.arctan2(dd, sd)
                    # if oc_angle >= -clip and oc_angle <= clip:

                    if self.name == 'ego':
                        temp_oc.append(sd)
                        temp_oc.append(dd)
                        temp_oc_flip.append(sd)
                        temp_oc_flip.append(dd)
                        # assign velocities
                        vs_rel  = np.minimum(10.0, np.maximum(-10.0, (sd - vehicle.previous_sd) / self.dt))
                        vd_rel  = np.minimum(10.0, np.maximum(-10.0, (dd - vehicle.previous_dd) / self.dt))
                        temp_oc.append(vs_rel)
                        temp_oc.append(vd_rel)
                        temp_oc_flip.append(-vs_rel)
                        temp_oc_flip.append(-vd_rel)
                        vehicle.previous_sd = sd
                        vehicle.previous_dd = dd

                    # Clip field of view to -45 to 45 degrees
                    xb, yb = sd + vehicle.base_s, dd + vehicle.base_d
                    d =  np.sqrt(xb**2 + yb**2)
                    idx = np.argmax(d)

                    xb   = np.delete(xb, idx)
                    yb   = np.delete(yb, idx)
                    d    = np.delete(d, idx)
                    location = np.arctan2(yb, xb)

                    ids = location.argsort()
                    d = d[ids]
                    location = location[ids]
                    # ADDED for eval
                    # loc = np.arctan2(dd, sd)
                    # new_size = 2 * (np.pi / 180)
                    # loc0 = loc - new_size if (loc - new_size) > -2 * np.pi else 4 * np.pi + (loc - new_size)
                    # loc2 = loc + new_size if (loc + new_size) < 2 * np.pi else -4 * np.pi + (loc + new_size)
                    # location = [loc0, loc, loc2]
                    # d =  np.sqrt(dd**2 + sd**2)

                    if (location[0] < self.clip and location[2] < self.clip) and (location[0] > -self.clip and location[2] > -self.clip):
                        if location[0] * location[2] < 0 and (location[0] > np.pi/2 or location[0] < -np.pi/2):
                            c = (self.phi < location[0])  & (self.phi > location[2])
                        else:
                            c = (self.phi > location[0])  & (self.phi < location[2])

                        # make linear change of distance
                        m_d = np.min(d)
                        a_d = (m_d * k + n)

                        self.pom_no_sigma[c]  = np.max([self.pom_no_sigma[c], np.ones_like(self.pom_no_sigma[c]) * a_d], axis=0)
                        self.pom[c]  = self.pom_no_sigma[c] + self.pom_gauss[c]

                if not self.maneuver_checked and self.t >= self.maneuver_start:
                    # check whether the selected maneuver is possible
                    if vehicle.s_ - self.s_ < 15 and vehicle.s_ - self.s_ > -3:
                        # check wether to look up or down when comparing vehicles
                        mult = 1
                        if self.maneuver == 2:
                            mult = -1
                        if abs(self.d_ + (mult * self.lane_width) - vehicle.d_) < self.half_lane / 2.0:
                            if self.maneuver != 0:
                                self.maneuver = 0
                                self.vd_ = 0
                                if self.debug:
                                    print(self.name + " - chg man: " + self.maneuvers[self.maneuver])

                    self.maneuver_checked = True
        # add lateral velocity with noise
        self.pom += np.abs(self.vd_  + vd_noise)

        # add data to OC list
        self.data_oc.append(temp_oc)
        self.data_oc_flip.append(temp_oc_flip)

    # Return generated scenario data
    def get_data(self):
        ns = np.array(self.s)
        nd = np.array(self.d)
        vs = np.array(self.vs)
        vd = np.array(self.vd)
        ss = np.array(self.sensor_s)
        sd = np.array(self.sensor_d)

        if self.add_noise:
            ns += np.random.normal(0, self.noise_std, size=np.array(self.s).shape)
            nd += np.random.normal(0, self.noise_std, size=np.array(self.s).shape)
            vs += np.random.normal(0, self.noise_std, size=np.array(self.s).shape)
            vd += np.random.normal(0, self.noise_std, size=np.array(self.s).shape)
            ss += np.random.normal(0, self.noise_std, size=np.array(self.s).shape)
            sd += np.random.normal(0, self.noise_std, size=np.array(self.s).shape)
        return  np.vstack((ns, nd, ss, sd)).T

    # Return raw PolFV
    def get_poms(self):
        poms =  np.array(self.poms)
        poms = cv2.resize(poms, (0,0), fx=0.125, fy=0.5, interpolation=cv2.INTER_AREA)
        return poms

    # Return PolFV with resolution adjustment
    def get_poms2(self):
        poms =  np.array(self.poms)

        poms_16_1 = cv2.resize(poms[:,0:150 * 8],       (0,0), fx=0.0625, fy=0.5, interpolation=cv2.INTER_AREA)
        poms_8_1  = cv2.resize(poms[:,150 * 8:170 * 8], (0,0), fx=0.125,  fy=0.5, interpolation=cv2.INTER_AREA)
        poms_f    = cv2.resize(poms[:,170 * 8:190 * 8], (0,0), fx=0.5,    fy=0.5, interpolation=cv2.INTER_AREA)
        poms_8_2  = cv2.resize(poms[:,190 * 8:210 * 8], (0,0), fx=0.125,  fy=0.5, interpolation=cv2.INTER_AREA)
        poms_16_2 = cv2.resize(poms[:,210 * 8:360 * 8], (0,0), fx=0.0625, fy=0.5, interpolation=cv2.INTER_AREA)

        poms = np.concatenate([poms_16_1, poms_8_1, poms_f, poms_8_2, poms_16_2], axis=1)
        return poms

    def get_label(self):
        return np.array(self.label)
