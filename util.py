import numpy as np
import matplotlib
import matplotlib.pyplot as plt


def cut_offset(output_data, offset):
    if offset <= 0:
        return output_data

    out_filling = np.zeros((offset, output_data.shape[1]), dtype=output_data.dtype)
    out_filling[:] = output_data[-1];

    output_data = np.concatenate((output_data[offset:,:], out_filling))

    return output_data

def cut_offset2(output_data, offset):
    if offset <= 0:
        return output_data

    out_filling = np.zeros_like(output_data[:,-offset:,:])

    output_data = np.concatenate((output_data[:,offset:], out_filling), axis=1)

    return output_data


def make_one_hot(labels, num_categ=9):
    return (np.eye(num_categ)[labels]).astype(np.float32)


def get_rf(kernels, strides, dilation):
    RF = []
    rf_ = 1
    si  = 1
    for k,s,d in zip(kernels, strides, dilation):
        rf_ = rf_ + (d * si * (k - 1))
        si *= s
        RF.append(rf_)
    return RF


def plot_highway(axes, s, lw=3.5):
    hlw = lw / 2.0
    axes.plot(s, np.ones_like(s) * hlw, '--', color='#222222')
    axes.plot(s, np.ones_like(s) * lw + hlw, '--', color='#222222')
    axes.plot(s, np.ones_like(s) * -hlw,'--', color='#222222')
    axes.plot(s, np.ones_like(s) * -(lw + hlw),'--', color='#222222')


def plot_scenario(dt, motion_vehicles, fig_size=(10, 2.0), sample_fq=5, save=None):
    f= plt.figure(figsize=fig_size)
    ax1  = plt.subplot(111)
    highway_end   = 0
    highway_start = motion_vehicles[0][0][0]

    for i, m in enumerate(motion_vehicles):
        m = m.squeeze()
        se = m[::sample_fq, 0]
        de = m[::sample_fq, 1]
        highway_end = max(highway_end, se[-1])
        if i == 0:
            ax1.scatter(se, de, c=se, cmap='winter', alpha=0.75)
        else:
            ax1.scatter(se, de, c=se, cmap='autumn', alpha=0.75)

    highway = np.linspace(highway_start, highway_end, len(se))
    plot_highway(ax1, highway)

    if save is not None:
        f.savefig("figures/" + save + "-scen.svg")
    plt.show()


def plot_scenario_oc(dt, data_oc, fig_size=(10, 2.0), sample_fq=5, save=False):
    f= plt.figure(figsize=fig_size)
    ax1  = plt.subplot(111)
    highway_end = 0

    se = [i[0] for i in data_oc]
    de = [i[1] for i in data_oc]
    ax1.scatter(se[::sample_fq], de[::sample_fq], c=se[::sample_fq], cmap='winter', alpha=0.75)
    highway_end = max(highway_end, se[-1])

    cmap = matplotlib.cm.get_cmap('autumn')

    len_data = len(data_oc)
    for i in range(0, len_data, sample_fq):
        num_elements = len(data_oc[i]) // 4
        if  num_elements > 1:
            for j in range(num_elements - 1):
                    s = se[i] + data_oc[i][4 + 4 * j]
                    d = de[i] + data_oc[i][5 + 4 * j]
                    highway_end = max(highway_end, s)
                    ax1.plot(s, d, 'o', c=cmap(i/len_data), alpha=0.75)

    highway = np.linspace(0, highway_end, len(se))
    plot_highway(ax1, highway)


def plot_labels(label, fig_size=(10, 2.0)):
    f= plt.figure(figsize=fig_size)
    ax1  = plt.subplot(111)
    highway_end = 0

    ax1.plot(label)
    plt.show()


def plot_pom(pom, fig_size=(10,10), save=None):
    f = plt.figure(figsize=fig_size)
    plt.imshow(pom.squeeze(), cmap='viridis')
    if save is not None:
        f.savefig("figures/" + save + ".svg")
    plt.show()
