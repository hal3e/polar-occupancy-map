import cv2
import numpy as np
from moviepy.editor import VideoFileClip
import matplotlib.pyplot as plt

class Annotate_video:
    def __init__(self, amps_, label_, label_names_, label_names_full_, offset_, input_file_):
        self.amps             = self.scale(amps_)
        self.un_s_amps        = amps_
        self.label            = label_
        self.label_names      = label_names_
        self.label_names_full = label_names_full_
        self.current_label    = label_names_full_[0]
        self.offset           = offset_

        self.polfv_steps = amps_.shape[0]
        self.polfv_sec   = self.polfv_steps // 10

        self.input_file  = input_file_
        print(self.input_file)
        self.output_file = input_file_[:-4] + '-seg.mp4'

        clip_temp = VideoFileClip(self.input_file)
        self.clip = clip_temp.subclip(2.0, self.polfv_sec)

        self.frame = 0
        self.move_up = 0
        self.move = 0
        self.frame_sync = 0
        self.frame_switcher = [2, 3, 3, 2, 2, 3, 3]
        self.size_for_labels = 80

        self.frame_amps = np.zeros((self.clip.size[1]//4 + self.size_for_labels//4, amps_.shape[1], 3), dtype=np.uint8)
        self.amps = np.concatenate((self.amps, np.zeros((self.frame_amps.shape[0]//2 + self.offset + self.size_for_labels//4, amps_.shape[1]))))
        self.cmap = plt.get_cmap('viridis')

        self.font = cv2.FONT_HERSHEY_SIMPLEX
    def create_frames(self, img):
        if(self.frame_sync == 0 or self.frame_sync == self.frame_switcher[(self.move_up + self.move) % 4]):

            self.frame_sync = 0
            amps_color = self.cmap(self.scale(self.un_s_amps[self.move_up + self.move : self.move_up + self.move + 2 * self.offset]), alpha=None, bytes=True)
            amps_color = np.delete(amps_color, 3, 2)

            if(amps_color.shape[0] != 2 * self.offset or self.move_up + self.move > len(self.label) - 1):
                return self.last_image

            a = self.amps[self.move : self.move + self.move_up + self.frame_amps.shape[0]//2 + self.offset]
            self.frame_amps[self.frame_amps.shape[0]//2 - self.offset - self.move_up:, :, :] = (np.repeat(a[:, :, np.newaxis], 3, axis=2) * 255).astype(np.uint8)

            self.frame_amps[self.frame_amps.shape[0]//2 - self.offset : self.frame_amps.shape[0]//2 + self.offset, :, :] = amps_color

            self.current_label = self.label_names_full[0]
            current_max = np.argmax(self.label[self.move_up + self.move])
            if self.label[self.move_up + self.move, current_max] >= 0.90:
                self.current_label = self.label_names_full[current_max]

            self.image_label = np.zeros((self.size_for_labels, 640 * 2, 3)) + 30
            self.add_label_text(self.label[self.move_up + self.move, :])

            if (self.move_up < self.frame_amps.shape[0]//2 - self.offset):
                self.move_up += 1
            else:
                self.move += 1

        frame_out = cv2.resize(self.frame_amps, (0,0), fx=1.0, fy=2.0, interpolation=cv2.INTER_CUBIC)
        self.create_white_boarder_amps(frame_out)

        # add small area  to write the current label
        img[-50:,:300, :] = 30
        cv2.putText(img, self.current_label, (50, img.shape[0]-10), self.font, 1.0, (255,255,255), 2, cv2.LINE_AA)
        img = cv2.resize(img, (0, 0), fx=0.5, fy=0.5, interpolation=cv2.INTER_CUBIC)

        image_label = cv2.resize(self.image_label, (0,0), fx=0.5, fy=0.5, interpolation=cv2.INTER_LINEAR)

        self.last_image = np.hstack((frame_out, np.vstack((img, image_label))))

        self.frame += 1
        self.frame_sync += 1

        return self.last_image

    def create_white_boarder_amps(self, img):
        img[img.shape[0]//2 + self.offset * 2, :, :] = 255
        img[img.shape[0]//2 - self.offset * 2, :, :] = 255
        img[img.shape[0]//2, ::27, :] = 255

    def annotate(self):
        out_clip = self.clip.fl_image(self.create_frames)
        out_clip.write_videofile(self.output_file, audio=True)

    def scale(self, amps):
        min = np.min(amps)
        max = np.max(amps)
        return (amps - min) /(max - min)

    def add_label_text(self, label_probab):
        final_string=""
        for i in range(len(label_probab)):
            final_string += self.label_names[i] + "{:.2f}".format(label_probab[i]) + " "
        cv2.putText(self.image_label, final_string, (10, 50), self.font, 0.9, (150,150,150), 2, cv2.LINE_AA)

def annotate_video(amps, label, label_names, label_names_full, offset, input_file):
    av = Annotate_video(amps, label, label_names, label_names_full, offset, input_file)
    av.annotate()
