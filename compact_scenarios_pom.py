import time
import numpy as np
import tensorflow as tf
from video_annotation import annotate_video
import matplotlib.pyplot as plt
from roc_auc import plot_roc_auc
from neural_network_model import make_model, make_model_vanilla_CNN
from util import plot_highway, plot_scenario, plot_labels, plot_pom, cut_offset2, make_one_hot
from generate_data_util import get_data, generate_batch_for_training, generate_batch_for_evaluation, generate_windows_from_real_data
# %%
scenario_labels = ['driving_in_lane', 'lane_change', 'freelane_cut_in', 'freelane_cut_out', 'cut-in-out',
                   'ta-free-cut-out', 'ta-free-cut-in', 'ta-sw-cut-in', 'ta-sw-cut-out']

data_file_name       = 'training/data-paper.npy'
show_random_scenario = True
show_all_scenarios   = False

load_model           = True
model_files          = './models/pom-50-m/model.ckpt'
train_model          = False
calc_valid_acc       = True  # Calculate the ROC and AUC for the model on the validation data
save_model           = False # When training use this to save the model in the models/new-model folder

num_steps   = 50  # Number of POM representations that will be used as input for the network
stride      = 1   # Stride for the sliding window
offset      = (num_steps // 2) - 1 # The network is making prediction in the middle of the window
num_classes = 9

with open(data_file_name, 'rb') as f:
    _       = np.load(f)
    _       = np.load(f)
    _       = np.load(f)
    X_train = np.load(f)
    X_test  = np.load(f)
    X_valid = np.load(f)
    y_train = np.load(f)
    y_test  = np.load(f)
    y_valid = np.load(f)
print("\n--- Loaded " + data_file_name + " ---")

# cut offset
y_train, y_test, y_valid  =  cut_offset2(y_train, offset), cut_offset2(y_test, offset), cut_offset2(y_valid, offset)

training_size = len(X_train)
test_size = len(X_test)
valid_size = len(X_valid)
print("Training size: {}".format(training_size))
print("Test size: {}".format(test_size))
print("Validation size: {}\n".format(valid_size))
#%%


#%%
# Show random scenario
if show_random_scenario:
    idx = np.random.choice(len(X_train))
    plot_pom(X_train[idx, :,:,0], fig_size=(10.0, 5.0))
    print("class: ", scenario_labels[np.max(np.argmax(y_train[idx], axis=1))])

    plt.plot(np.argmax(y_train[idx], axis=1))
    plt.show()
#%%


# Show all scenarios
if show_all_scenarios:
    for x in X_train:
        plot_pom(x)

# %%
# Make the neural network model
tf.reset_default_graph()
X, Y, train_op_sup, cost_sup, c_soft, dropout, learning_rate = make_model(num_steps=num_steps, phi_size=270, num_classes=num_classes)
# X, Y, train_op_sup, cost_sup, c_soft, dropout, learning_rate = make_model_vanilla_CNN(num_steps=num_steps, phi_size=270, num_classes=num_classes)

config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.95
sess = tf.Session(config=config)
saver = tf.train.Saver()

# Restore a model checkpoint or randomly initialize weights
if load_model:
    saver.restore(sess, model_files)
else:
    sess.run(tf.global_variables_initializer())
# %%

# Train the model ------------------------------------------------------------------------------------------------------------------
if train_model:
    print("--- Running Training ---")
    batch_size     = 128
    training_steps = 256 * 9
    display_step   = 250
    l_rate_        = 0.0001
    dropout_       = 0.9 # keep probab
    mb_loss        = 0
    t_acc          = 0

    # Create generator
    next_batch =  generate_batch_for_training(X_train, y_train, batch_size=batch_size, num_steps=num_steps, stride=stride)

    start_time = time.time()
    disp_time  = time.time()
    for step in range(1, training_steps+1):
        batch_x, batch_y = next(next_batch)
        batch_y = batch_y.squeeze()

        _, b_loss, ls_cs = sess.run([train_op_sup, cost_sup, c_soft], feed_dict={X: batch_x, Y: batch_y, dropout: dropout_, learning_rate: l_rate_})
        mb_loss += b_loss / display_step

        b_acc_t = np.sum(np.argmax(ls_cs, axis=1) == np.argmax(batch_y, axis=1)) / batch_size
        t_acc += b_acc_t / display_step
        if step % display_step == 0:
            c_time = time.time()
            print("Step {}, Mean batch Loss= {:.4f}, time: {:.2f}, lrate: {:.2e}".format(step, mb_loss, c_time - disp_time, l_rate_))
            disp_time = c_time
            print("t acc: ", round(t_acc,4))

            # Evaluate accuracy on the test dataset
            b_counter = 0
            b_acc = 0
            batch_size_eval = 256
            next_batch_eval =  generate_batch_for_evaluation(X_test, y_test, batch_size=batch_size_eval, num_steps=num_steps, stride=stride)
            for x_e, y_e in next_batch_eval:
                y_e = y_e.squeeze()
                ls_cs = sess.run(c_soft, feed_dict={ X: x_e, Y: y_e, dropout: 1.0})
                b_acc += np.sum(np.argmax(ls_cs, axis=1) == np.argmax(y_e, axis=1)) / batch_size_eval
                b_counter += 1

            print("v acc: ", round(b_acc/b_counter,4))
            mb_loss = 0
            t_acc = 0
        # decay learning rate
        l_rate_ *= 0.99975

    elapsed_time = time.time() - start_time
    print("--- Training finished in: {:.2f}s ---".format(elapsed_time))

    if save_model:
        save_path = saver.save(sess, "./models/new-model-pom-50-m/model.ckpt")
        print("--- Model saved in file: {:s} ---".format(save_path))
# %%

# Get validation accuracy
if calc_valid_acc:
    print("--- Evaluating model on validation data ---")
    b_counter = 0
    b_acc = 0
    batch_size_eval = 256
    next_batch_eval =  generate_batch_for_evaluation(X_valid, y_valid, batch_size=batch_size_eval, num_steps=num_steps, stride=stride)
    y_val_c = []
    y_val = []
    for x_e, y_e in next_batch_eval:
        y_e = y_e.squeeze()
        y_val.append(y_e)
        ls_cs = sess.run(c_soft, feed_dict={ X: x_e, Y: y_e, dropout:1.0})
        y_val_c.append(ls_cs)
        sum = np.sum(np.argmax(ls_cs, axis=1) == np.argmax(y_e, axis=1))
        b_acc += np.sum(np.argmax(ls_cs, axis=1) == np.argmax(y_e, axis=1)) / batch_size_eval
        b_counter += 1

    print("accuracy: ", round(b_acc/b_counter,3))

    y_val_c = np.vstack(y_val_c)
    y_val = np.vstack(y_val)

    to_plot_c = np.argmax(y_val_c, axis=1)
    to_plot_y = np.argmax(y_val, axis=1)

    plot_roc_auc(y_val, y_val_c, n_classes=num_classes, show_zoom=False, latex_style=False)

scenario_labels = ['driving_in_lane', 'lane_change', 'freelane_cut_in', 'freelane_cut_out', 'cut-in-out',
                   'tof-free-cut-out', 'tof-free-cut-in', 'tof-sw-cut-in', 'tof-sw-cut-out']
# %%

# Evaluate on real data
input_data  = 'training/graz-181115-75m-pom-50.npy'
input_video = './videos/graz-181115.mp4'

with open(input_data, 'rb') as f:
    X_real = np.load(f)

X_real = np.expand_dims(X_real, axis=0)
X_real = np.expand_dims(X_real, axis=3)

len_real = X_real.shape[1]
y_real   = np.zeros(len_real)

# generate labels for real data
y_real[-offset + 46  : -offset + 95]   = 4.0
y_real[-offset + 585 : -offset + 625]  = 7.0
y_real[-offset + 650 : -offset + 679]  = 3.0
y_real[-offset + 679 : -offset + 690]  = 1.0
y_real[-offset + 755 : -offset + 805]  = 6.0
y_real[-offset + 2060: -offset + 2085] = 6.0
y_real[-offset + 2630: -offset + 2680] = 2.0
y_real[-offset + 2955:]                = 4.0
y_real = y_real[:-(num_steps - 1 )]

# show_steps = True
show_steps = False
break_step = 1500000
# break_step = 15

next_batch_eval =  generate_windows_from_real_data(X_real, num_steps=num_steps, stride=stride)
to_plot_c = []
counter = 0
for x_e in next_batch_eval:
    ls_cs = sess.run(c_soft, feed_dict={ X: x_e, dropout:1.0})
    to_plot_c.append(ls_cs)
    if show_steps:
        print("step: {}, ls: {}".format(counter, np.round(ls_cs,1)))
        copy_x_e = np.copy(x_e)
        copy_x_e[0, offset, ::2] = 0.5
        plot_pom(copy_x_e[0])
        counter += 1
        if counter > break_step:
            break

# %%
real_data_probab = np.vstack(to_plot_c)
to_plot_c = np.copy(real_data_probab)
# to_plot_c[to_plot_c < 0.90] = 0.0
to_plot_c2 = np.argmax(to_plot_c, axis=1)
real_data_probab = np.round(real_data_probab, 2)
print("--- done ---")
# %%

scenario_labels = ['driving_in_lane', 'lane_change', 'freelane_cut_in', 'freelane_cut_out', 'cut-in-out',
                   'ta-free-cut-out', 'ta-free-cut-in', 'ta-sw-cut-in', 'ta-sw-cut-out']

f = plt.figure(figsize=(20,5))
plt.plot(to_plot_c2, alpha=0.3)
plt.plot(y_real, alpha=0.7)

r_acc = np.sum(to_plot_c2 == y_real) / len_real
print("accuracy on real data: ", r_acc)

plot_roc_auc(make_one_hot(y_real.astype(np.int)) , make_one_hot(to_plot_c2.astype(np.int)), n_classes=num_classes, show_zoom=False, latex_style=False)
# %%
# 0. driving_in_lane
# 1. lane_change
# 2. freelane_cut_in
# 3. freelane_cut_out
# 4. cut-in-out
# 5. ta-free-cut-out
# 6. ta-free-cut-in
# 7. ta-sw-cut-in
# 8. ta-sw-cut-out

X_test_video = X_real.squeeze()[:]

scenario_labels_short = ['dil:', 'lc:', 'fci:', 'fco:', 'cio:', 'taco:', 'taci:', 'tasci:', 'tasco:']
annotate_video(X_test_video, real_data_probab, scenario_labels_short, scenario_labels,  offset, input_video)
