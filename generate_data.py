import numpy as np
import matplotlib.pyplot as plt
from util import plot_highway, plot_scenario, plot_scenario_oc, plot_labels, plot_pom, cut_offset2
from generate_data_util import get_data, generate_batch_for_training, generate_batch_for_evaluation, generate_windows_from_real_data
from og_rep import OG
# %%
scenario_labels = ['driving_in_lane', 'lane_change', 'freelane_cut_in', 'freelane_cut_out', 'cut-in-out',
                   'ta-free-cut-out', 'ta-free-cut-in', 'ta-sw-cut-in', 'ta-sw-cut-out']

# Program Flags--------------------------------------------------------------------------------
data_size            = 6000 # Number of scenarios that will be generated
data_file_name       = 'training/data-new.npy'
show_random_scenario = True
show_all_scenarios   = False

print("\n--- Generating new data ---\n")

temp_data = get_data(size_train=data_size, test_p=0.1, val_p=0.1, visualization=False, use_flip_augmentation=True)
X_train_raw, X_test_raw, X_valid_raw, X_train, X_test, X_valid, y_train, y_test, y_valid = temp_data

training_size = len(X_train)
test_size = len(X_test)
valid_size = len(X_valid)

with open(data_file_name, 'wb') as f:
    np.save(f, X_train_raw)
    np.save(f, X_test_raw)
    np.save(f, X_valid_raw)
    np.save(f, X_train)
    np.save(f, X_test)
    np.save(f, X_valid)
    np.save(f, y_train)
    np.save(f, y_test)
    np.save(f, y_valid)

print("\n--- Saved ---\n")

print("Training size: {}".format(training_size))
print("Test size: {}".format(test_size))
print("Validation size: {}\n".format(valid_size))
#%%

# Show random scenario
if show_random_scenario:
    og = OG()
    idx = np.random.choice(len(X_train))
    plot_scenario_oc(0.125, X_train_raw[idx])
    plot_pom(X_train[idx, :,:,0], fig_size=(10.0, 5.0))
    plt.imshow(og.construct_hig(X_train_raw[idx])[:,:,0])
    plt.show()
    plt.imshow(og.construct_hig(X_train_raw[idx])[:,:,1])
    plt.show()
    plt.imshow(og.construct_hig(X_train_raw[idx])[:,:,2])
    plt.show()
    print("class: ", scenario_labels[np.max(np.argmax(y_train[idx], axis=1))])

    plt.plot(np.argmax(y_train[idx], axis=1))
    plt.show()
#%%

# Show all scenarios
if show_all_scenarios:
    for x in X_train:
        plot_pom(x)
#%%
